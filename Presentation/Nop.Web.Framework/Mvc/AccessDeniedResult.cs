﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Web.Framework.Mvc
{
    public class AccessDeniedResult : RedirectToActionResult
    {
        public AccessDeniedResult(string actionName, string controllerName, object routeValues)
            : base(actionName, controllerName, routeValues)
        {

        }
    }
}
